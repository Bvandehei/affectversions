import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class GetTopProjects {

   public static void main(String[] args) throws IOException {

      String csvSplitBy = ",";
      ArrayList<String> top = new ArrayList<String>();

      try (BufferedReader br = new BufferedReader(new FileReader("OrderedQualityModel.csv"))) {
         Integer sum = 0;
         String linefromcsv = br.readLine();
         while ( (linefromcsv = br.readLine()) != null && sum < 100) {
            String[] token = linefromcsv.split(csvSplitBy);
            Double bugs= Double.parseDouble(token[1]);
            Double versions= Double.parseDouble(token[2]);
            Double good= Double.parseDouble(token[token.length-1]);
            if (bugs >= 100 && versions>= 6 && good < 0.51){
               sum++;
               top.add(token[0]);
            }
         }
      } catch (IOException e) {
         e.printStackTrace();
      }

      FileWriter fileWriter = null;
			try {
         fileWriter = null;
         String outname = "TopProjects.csv";

         fileWriter = new FileWriter(outname);
         try (BufferedReader br = new BufferedReader(new FileReader("Top100Projects.csv"))) {
            String linefromcsv = br.readLine();
            while ( (linefromcsv = br.readLine()) != null ) {
               String[] token = linefromcsv.split(csvSplitBy);
               if (top.contains(token[0])){
                  fileWriter.append(linefromcsv+"\n");
               }
            }
         } catch (IOException e) {
            e.printStackTrace();
         }
      } catch (Exception e) {
         System.out.println("Error in csv writer");
         e.printStackTrace();
      } finally {
         try {
            fileWriter.flush();
            fileWriter.close();
         } catch (IOException e) {
            System.out.println("Error while flushing/closing fileWriter !!!");
            e.printStackTrace();
         }
      }

   }
}
