import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class ComputeMetrics {

   public static boolean isNumeric(String str) {
     try {
         Integer.parseInt(str);
         return true;
      } catch(NumberFormatException e){
         return false;
      }
   }

   public static void main(String[] args) throws IOException {
      String command = "";
      String project = args[0];
      String gitrepo = args[1];
      String[] token2 = gitrepo.split("/");
      String git =  token2[token2.length - 1].substring(0, token2[token2.length - 1].length() - 4);
      System.out.println(project + " " + git);
      String csvSplitBy = ",";
      ArrayList<String> fixCommitFiles = new ArrayList<String>();
      HashMap<Integer, String> releaseNames = new HashMap<Integer, String>();
      HashMap<Integer, String> releaseDates = new HashMap<Integer, String>();
      HashMap<Integer, String> lastCommits = new HashMap<Integer, String>();
      try (BufferedReader br = new BufferedReader(new FileReader("./BugFiles/"+project+"BugInfo.csv"))) {
         String linefromcsv;
         while ( (linefromcsv = br.readLine()) != null) {
            String[] token = linefromcsv.split(csvSplitBy);
            if (token[9].equals(git) && !fixCommitFiles.contains(token[8]))
               fixCommitFiles.add(token[8]);
         }
      } catch (IOException e) {
         e.printStackTrace();
      }

      String versionfilename = "VersionFiles/"+ project + "VersionInfo.csv";
      /*Get Versions info for each project*/
      try (BufferedReader brVersions = new BufferedReader(new FileReader(versionfilename))) {
         brVersions.readLine();
         String linefromversion;
         while ( (linefromversion = brVersions.readLine()) != null) {
            String[] tokenVersion = linefromversion.split(csvSplitBy);
            releaseDates.put(Integer.parseInt(tokenVersion[0]), tokenVersion[3]);
            releaseNames.put(Integer.parseInt(tokenVersion[0]), tokenVersion[2]);
            command = "git --git-dir ./" + git + "/.git " +
                       "log --until " + tokenVersion[3] + " --pretty=format:%H -1";
            Process proc = Runtime.getRuntime().exec(command);
            BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            lastCommits.put(Integer.parseInt(tokenVersion[0]), reader.readLine());
            proc.waitFor();
         }
      } catch (Exception e) {
         e.printStackTrace();
      }

         FileWriter fileWriter = null;
         try {
				    fileWriter = new FileWriter(project+"_Metrics.csv");
            fileWriter.append("Version,File Name,LOC,LOC_touched,NR,NFix,NAuth,LOC_added,MAX_LOC_added,"+
             "AVG_LOC_added,Churn,MAX_Churn,AVG_Churn,ChgSetSize,MAX_ChgSet,AVG_ChgSet,Age,WeightedAge\n");
            String versionRelease = "1990-01-01T00:00";
            for (Integer versionI = 1; versionI < ((releaseNames.size()/2.0) + 1.0); versionI++) {
               String startDate = versionRelease;
               String versionName = releaseNames.get(versionI);
               versionRelease = releaseDates.get(versionI);
               String lastCommit = lastCommits.get(versionI);
               command = "git --git-dir ./" + git + "/.git " +
                       "--work-tree ./" +git+ "/ checkout " + lastCommit;
               Process proc = Runtime.getRuntime().exec(command);
               proc.waitFor();

               command = "git --git-dir ./" + git + "/.git " +
                "ls-tree --full-tree -r --name-only HEAD";
               proc = Runtime.getRuntime().exec(command);
               BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
				       String line = "";
               ArrayList<String> files = new ArrayList<String>();
				       while((line = reader.readLine()) != null) {
                  files.add(line);
               }
               proc.waitFor();

               HashMap<String, Integer> numFilesPerCommit = new HashMap<String, Integer>();
               command = "git --git-dir ./" + git + "/.git log --since "
                + startDate +" --until " + versionRelease + " --stat --" +
                "pretty=format:%H | grep -E \'^\\w|files? changed\' | awk -F \' \' \'{print $1}\'";
               proc = Runtime.getRuntime().exec(new String[] {"/bin/sh", "-c", command});
               reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
               Integer count = 1;
               String hash = "";
               while((line = reader.readLine()) != null) {
                  if ( count % 2 == 1) {
                     hash = line;
                     count++;
                  }
                  else {
                     if (isNumeric(line)) {
                        numFilesPerCommit.put(hash,Integer.parseInt(line));
                        count++;
                     }
                     else {
                        numFilesPerCommit.put(hash, 0);
                        hash = line;
                     }
                  }
               }
               proc.waitFor();

				       for ( int i = 0; i < files.size(); i++) {
                  String filename = files.get(i);
                  String filenameQ = files.get(i).replace("'", "\\'");

                  /*Get LOC*/
                  if (!filename.equals(filenameQ))
                     command = "cat \"./" + git + "/" + filename + "\" | sed \'/^\\s*$/d\' | wc -l";
                  else
                     command = "cat \'./" + git + "/" + filename + "\' | sed \'/^\\s*$/d\' | wc -l";
				          proc = Runtime.getRuntime().exec(new String[] {"/bin/sh", "-c", command});
				          reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
				          String LOC = reader.readLine().trim();
                  proc.waitFor();

                  /*Get Age in weeks*/
                  if (!filename.equals(filenameQ)) {
                     command = "git --git-dir ./"+git+"/.git log --diff-filter=A "
                      + "--follow --format=%ai -- \"" + filename + "\" | tail -1";
                  } else {
                     command = "git --git-dir ./"+git+"/.git log --diff-filter=A "
                      + "--follow --format=%ai -- \'" + filename + "\' | tail -1";
                  }
				          proc = Runtime.getRuntime().exec(new String[] {"/bin/sh", "-c", command});
				          reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                  String addDateStr = "";
                  if ((addDateStr = reader.readLine()) == null) {
                     proc.waitFor();
                     if (!filename.equals(filenameQ)) {
                        command = "git --git-dir ./"+git+"/.git log "
                         + "--follow --format=%ai -- \"" + filename + "\" | tail -1";
                     } else {
                        command = "git --git-dir ./"+git+"/.git log "
                         + "--follow --format=%ai -- \'" + filename + "\' | tail -1";
                     }
				             proc = Runtime.getRuntime().exec(new String[] {"/bin/sh", "-c", command});
				             reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));

                     if ((addDateStr = reader.readLine()) == null) {
                        proc.waitFor();
                        if (!filename.equals(filenameQ)) {
                          command = "git --git-dir ./"+git+"/.git log "
                           + " --format=%ai -- \"" + filename + "\" | tail -1";
                        } else {
                          command = "git --git-dir ./"+git+"/.git log "
                           + " --format=%ai -- \'" + filename + "\' | tail -1";
                        }
				                proc = Runtime.getRuntime().exec(new String[] {"/bin/sh", "-c", command});
				                reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
				                addDateStr = reader.readLine();
                     }
                  }
				          addDateStr = addDateStr.trim().substring(0,19).replace(" ", "T");
                  proc.waitFor();

                  LocalDateTime addDate = LocalDateTime.parse(addDateStr);
                  LocalDateTime releaseDate = LocalDateTime.parse(versionRelease);
                  Double age = ChronoUnit.DAYS.between(addDate,releaseDate) / 7.0;

                  /*Get number of authors*/
                  if (!filename.equals(filenameQ)) {
                     command = "git --git-dir ./"+git+"/.git log --since "
                      + startDate + " --until " + versionRelease+ " --follow "
                      + "--pretty=format:%an --  \""+filename+"\" | sort -u | wc -l";
                  }
                  else {
                     command = "git --git-dir ./"+git+"/.git log --since "
                      + startDate + " --until " + versionRelease+ " --follow "
                      + "--pretty=format:%an --  \'"+filename+"\' | sort -u | wc -l";
                  }
				          proc = Runtime.getRuntime().exec(new String[] {"/bin/sh", "-c", command});
				          reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
				          String numAuthors = reader.readLine().trim();
                  proc.waitFor();

                  /*Get insertions and deletions etc.*/
                  if (!filename.equals(filenameQ)) {
                     command = "git --git-dir ./"+git+"/.git log --since "
                      + startDate + " --until " + versionRelease+ " --stat --follow "
                      + "--pretty=format:\',%H %ai,\' --  \""+filename+"\" | grep -E \'^,\\w|files? "
                      + "changed\' | awk -F \',\' \'{print $2} {print $3}\' | grep \'\\w\'";
                  } else {
                     command = "git --git-dir ./"+git+"/.git log --since "
                      + startDate + " --until " + versionRelease+ " --stat --follow "
                      + "--pretty=format:\',%H %ai,\' --  \'"+filename+"\' | grep -E \'^,\\w|files? "
                      + "changed\' | awk -F \',\' \'{print $2} {print $3}\' | grep \'\\w\'";
                  }
                  proc = Runtime.getRuntime().exec(new String[] {"/bin/sh", "-c", command});
                  reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                  hash = "";
                  Integer thisInsertion = 0;
                  Integer thisDeletion = 0;
                  Integer totInsertions = 0;
                  Integer totDeletions = 0;
                  Integer totalCommits = 0;
                  Integer sumChurn = 0;
                  Integer maxChurn = -100000000;
                  Integer numBugFixes = 0;
                  Integer maxInsertion = 0;
                  Integer sumCommittedTogether = 0;
                  Integer maxCommittedTogether = 0;
                  Double weightedAgeSum = 0.0;
                  String expecting = "hash";
                  LocalDateTime thisDate = releaseDate;
                  while((line = reader.readLine()) != null) {
                     line = line.trim();
                     Integer found = 0;
                     if ( expecting.equals("insertion")) {
                        if ( line.indexOf("insertion") != -1) {
                           String[] temp = line.split(" ");
                           totInsertions += Integer.parseInt(temp[0]);
                           thisInsertion = Integer.parseInt(temp[0]);
                           if (thisInsertion > maxInsertion)
                               maxInsertion = thisInsertion;
                           found = 1;
                        }
                        expecting = "deletion";
                     }
                     if (expecting.equals("deletion") && found == 0) {
                        if (line.indexOf("deletion") != -1){
                           String[] temp = line.split(" ");
                           totDeletions += Integer.parseInt(temp[0]);
                           thisDeletion = Integer.parseInt(temp[0]);
                           found = 1;
                        }
                        Integer thisChurn = thisInsertion - thisDeletion;
                        if (thisChurn > maxChurn)
                           maxChurn = thisChurn;
                        sumChurn += thisChurn;
                        expecting = "hash";

                        /* calculate weighted Age*/
                        Integer thisLOCTouched = thisInsertion + thisDeletion;
                        Double ageToRelease = ChronoUnit.DAYS.between(addDate,thisDate) / 7.0;
                        weightedAgeSum += (ageToRelease * thisLOCTouched);
                     }
                     if (expecting.equals("hash") && found == 0) { // a hash
                        /* reset insertion/ deletion*/
                        thisDeletion = 0;
                        thisInsertion = 0;
                        String [] hashdate = line.split(" ");
                        hash = hashdate[0];
                        thisDate = LocalDateTime.parse((hashdate[1] + "T" + hashdate[2]));
                        /*check if hash is a bug fixing hash*/
                        if (fixCommitFiles.contains(hash))
                           numBugFixes++;
                        /*increment number of revisions for each hash of a commit found*/
                        totalCommits++;
                        /* get total change set size*/
                        sumCommittedTogether += numFilesPerCommit.get(hash);
                        /* get max change set size*/
                        if (numFilesPerCommit.get(hash)> maxCommittedTogether)
                           maxCommittedTogether = numFilesPerCommit.get(hash);
                        expecting = "insertion";
                     }
                  }
                  if ( !expecting.equals("hash")) {
                     Integer thisChurn = thisInsertion - thisDeletion;
                     if (thisChurn > maxChurn)
                        maxChurn = thisChurn;
                     sumChurn += thisChurn;

                     Integer thisLOCTouched = thisInsertion + thisDeletion;
                     Double ageToRelease = ChronoUnit.DAYS.between(addDate,thisDate) / 7.0;
                     weightedAgeSum += (ageToRelease * thisLOCTouched);
                  }
                  if (maxChurn == -100000000) {
                     maxChurn = 0;
                  }

                  proc.waitFor();
                  Integer totLOC_Touched = totInsertions + totDeletions;
                  Integer changed = 0;
                  if(totalCommits == 0) {
                    totalCommits = 1;
                    changed = 1;
                  }
                  Double weightedAge = 0.0;
                  if (totLOC_Touched != 0)
                     weightedAge = weightedAgeSum / totLOC_Touched;
                  /* get all the averages by dividing by number of commits (revisions)*/
                  Double avgLOC_Added = (double)totInsertions / (double) totalCommits;
                  Double avgChurn = (double)sumChurn / (double) totalCommits;
                  Double avgCommittedTogether = (double)sumCommittedTogether / (double) totalCommits;
                  if (changed == 1)
                     totalCommits = 0;
                  fileWriter.append(versionI.toString() + "," + filename + "," + LOC + ","+
                   totLOC_Touched + "," + totalCommits +","+ numBugFixes+ ","+
                   numAuthors + ","+ totInsertions+ "," + maxInsertion+ "," +
                   avgLOC_Added+ "," + sumChurn+ ","+ maxChurn+ "," + avgChurn+
                   "," + sumCommittedTogether+ "," + maxCommittedTogether+ "," +
                   avgCommittedTogether + "," + age + "," + weightedAge+"\n");
               }
            }
         } catch (Exception e) {
            System.out.println(command);
            System.out.println("Error in csv writer");
            e.printStackTrace();
         } finally {
            try {
               fileWriter.flush();
               fileWriter.close();
            } catch (IOException e) {
               System.out.println("Error while flushing/closing fileWriter !!!");
               e.printStackTrace();
            }
         }
   }
}
