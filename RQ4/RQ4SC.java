import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.math.*;

public class RQ4SC {
    public static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
   public static void main(String[] args) throws IOException, InterruptedException {
      String csvSplitBy = ",";
      String title = "Simple,SZZ_B+,SZZ_U+,SZZ_RA+,SZZ_B,SZZ_U,SZZ_RA,Proportion_ColdStart,Proportion_Increment,Proportion_MovingWindow";
      String [] methods = title.split(csvSplitBy);
      String title2 = "Version,File Name,LOC,LOC_touched,NR,NFix,NAuth,LOC_added,MAX_LOC_added,AVG_LOC_added,Churn,MAX_Churn,AVG_Churn,ChgSetSize,MAX_ChgSet,AVG_ChgSet,Age,WeightedAge,Buggy";
      String [] features = title2.split(csvSplitBy);
      try (BufferedReader br = new BufferedReader(new FileReader("../RQ1/ProjectDetails.csv"))) {
         String lineProjects = br.readLine();
         while ( (lineProjects = br.readLine()) != null ) {
           String[] token = lineProjects.split(csvSplitBy);
           String project = token[0];
           int versions = Integer.parseInt(token[7]);
           String path =  "";
           for (Integer m = 0; m < methods.length; m++) {
             for (Integer r = 2; r < versions; r++) {
               for (Integer c = 2; c<18; c++) {
                 ArrayList<Double> items = new ArrayList<Double>();
                 ArrayList<Double> def = new ArrayList<Double>();
                 int numYM = 0, numYA=0;
				         try (BufferedReader brMetrics = new BufferedReader(new FileReader("../RQ3/Complete/"+ project +"_" + methods[m] + "_Complete.csv"))) {
				            String linefromcsv = brMetrics.readLine();
                    Boolean gt = false;
				            while ( (linefromcsv = brMetrics.readLine()) != null && gt == false ) {
				               String[] tokenMetrics = linefromcsv.split(csvSplitBy);
				               if (tokenMetrics.length != 19)
                         System.out.println("ERROR LINE NOT 19");
                       else if (Integer.parseInt(tokenMetrics[0]) <= r){
                          if (!(isNumeric(tokenMetrics[3]) || isNumeric(tokenMetrics[4]))){

                          }
                          else if (isNumeric(tokenMetrics[2])){
                            items.add(Double.parseDouble(tokenMetrics[c]));
                            if (tokenMetrics[18].equals("Yes")){
                              def.add(1.0);
                              numYM++;
                            }
                            else if (tokenMetrics[18].equals("No"))
                              def.add(0.0);
                            else
                              System.out.println("not a buggy column");
                          } else {
                            if (c != 17)
                              items.add(Double.parseDouble(tokenMetrics[c+1]));
                            else
                              items.add(0.0);
                            if (tokenMetrics[18].equals("Yes")){
                              numYM++;
                              def.add(1.0);
                            }
                            else if (tokenMetrics[18].equals("No"))
                              def.add(0.0);
                            else
                              System.out.println("not a buggy column");
                          }
                       } else {
                         gt = true;
                       }
				            }
				         } catch (IOException e) {
				           e.printStackTrace();
				         }
                 ArrayList<Double> itemsA = new ArrayList<Double>();
                 ArrayList<Double> defA = new ArrayList<Double>();
				         try (BufferedReader brMetrics = new BufferedReader(new FileReader("../RQ3/Complete/"+ project +"_Actual_Complete.csv"))) {
				            String linefromcsv = brMetrics.readLine();
                    Boolean gt = false;
				            while ( (linefromcsv = brMetrics.readLine()) != null && gt == false ) {
				               String[] tokenMetrics = linefromcsv.split(csvSplitBy);
				               if (tokenMetrics.length != 19)
                         System.out.println("ERROR LINE NOT 19");
                       else if (Integer.parseInt(tokenMetrics[0]) <= r){
                          if (!(isNumeric(tokenMetrics[3]) || isNumeric(tokenMetrics[4]))){

                          }
                          else if (isNumeric(tokenMetrics[2])){
                            itemsA.add(Double.parseDouble(tokenMetrics[c]));
                            if (tokenMetrics[18].equals("Yes")){
                              defA.add(1.0);
                              numYA++;
                            }
                            else if (tokenMetrics[18].equals("No"))
                              defA.add(0.0);
                            else
                              System.out.println("not a buggy column");
                          } else {
                            if (c != 17)
                              itemsA.add(Double.parseDouble(tokenMetrics[c+1]));
                            else
                              itemsA.add(0.0);
                            if (tokenMetrics[18].equals("Yes")){
                              numYA++;
                              defA.add(1.0);
                            }
                            else if (tokenMetrics[18].equals("No"))
                              defA.add(0.0);
                            else
                              System.out.println("not a buggy column");
                          }
                       } else {
                         gt = true;
                       }
				            }
				         } catch (IOException e) {
				           e.printStackTrace();
				         }
                 if (numYA>0 && numYM>0){
                 double[] a1 = new double[items.size()];
                 for (int i = 0; i < a1.length; i++) {
                   a1[i] = items.get(i);
                 }
                 double[] a2 = new double[def.size()];
                 for (int i = 0; i < a2.length; i++) {
                   a2[i] = def.get(i);
                 }
                 double[] a3 = new double[itemsA.size()];
                 for (int i = 0; i < a3.length; i++) {
                   a3[i] = itemsA.get(i);
                 }
                 double[] a4 = new double[defA.size()];
                 for (int i = 0; i < a4.length; i++) {
                   a4[i] = defA.get(i);
                 }
                 SpearmansCorrelation sc = new SpearmansCorrelation();
                 double correlation = sc.correlation(a1,a2);
                 SpearmansCorrelation scA = new SpearmansCorrelation();
                 double correlationA = scA.correlation(a3,a4);
                 double diff = Math.abs(correlation-correlationA);
System.out.println(diff + "," + correlation+ ","+correlationA);
                 if (correlationA != 0 )
                   diff = diff/correlationA;
                 else if (diff ==0)
                   diff = 0;
                 else
                   diff = 1;
                  System.out.println(project+ "," + r+ "," + methods[m] + "," + features[c]+","+diff);
                 }
               }
             }
          }
        }
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
}
