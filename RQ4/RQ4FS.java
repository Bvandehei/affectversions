import weka.core.converters.ConverterUtils.DataSource;
import weka.core.Instances;
import weka.classifiers.Classifier;
//import weka.classifiers.AbstractClassifier;
import weka.classifiers.Evaluation;
import java.util.Random;
import java.util.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.*;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.supervised.instance.Resample;
import weka.filters.Filter;
import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.*;
import weka.attributeSelection.ASSearch.*;
import weka.classifiers.trees.RandomForest;
import java.io.IOException;
import weka.core.Attribute;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.Logistic;

public class RQ4FS {

   public static void main(String[] args) throws Exception, IOException{

      try (BufferedReader br = new BufferedReader(new FileReader("../../RQ1/ProjectDetails.csv"))) {

       FileWriter fileWriter2 = null;
       FileWriter fileWriter1 = null;
       try {
			   fileWriter1 = new FileWriter("SelectedAttributes.csv");
			   fileWriter2 = new FileWriter("RQ4Statistics.csv");
				 fileWriter2.append("project" + "," + "release" + ",classifier," + "method"+ "," + "TP"
		                             + "," + "TN" + "," + "FP" + "," +"FN" + ","
		                             + "precision" + "," + "recall" + "," + "F1" +"," + "MCC" + "," + "kappa,auc" + "\n");
				 fileWriter1.append("project" + "," + "release" + "," + "method"+ "," + "Attributes" + "\n");
         String lineProjects = br.readLine();
         String csvSplitBy = ",";
         String methodsb = "Actual,Simple,SZZ_B+,SZZ_U+,SZZ_RA+,SZZ_B,SZZ_U,SZZ_RA,Proportion_ColdStart,Proportion_Increment,Proportion_MovingWindow";
         String [] methods = methodsb.split(csvSplitBy);
         while ( (lineProjects = br.readLine()) != null ) {

            String[] token = lineProjects.split(csvSplitBy);
            String project = token[0];
            System.out.println(token[0]);
            int versions = Integer.parseInt(token[7]);
            double incr = (double)(versions) *0.05;
            if (versions <= 20.0)
              incr = 1.0;
            double start = incr;
            while (start < 2.0)
              start += incr;

            for (double releaseIncr=start; releaseIncr<(double)(versions) || Math.abs(versions - releaseIncr) < 0.4 ; releaseIncr+=incr) {
                int release = (int)Math.round(releaseIncr);
					      try (BufferedReader brMetrics = new BufferedReader(new FileReader("../../RQ3/CompleteMultiplied/"+project+ "_Actual_Complete.csv"))) {
					         FileWriter fileWriter = null;
					         try {
									    fileWriter = new FileWriter(project+"_Actual_Complete_"+release+"_Test.csv");
					            String linefromcsv = brMetrics.readLine();
					            fileWriter.append(linefromcsv.substring(18,linefromcsv.length())+ "\n");
					            while ( (linefromcsv = brMetrics.readLine()) != null) {
					               String[] tokenMetrics = linefromcsv.split(csvSplitBy);
					               int versionIndex = Integer.parseInt(tokenMetrics[0]);
					               if ( versionIndex == release) {
                           for (int n = 2; n < 18; n++)
				                     fileWriter.append(tokenMetrics[n] + ",");
                           fileWriter.append(tokenMetrics[18] + "\n");
					               }
					            }
					         } catch (Exception e) {
					            System.out.println("Error in csv writer");
					            e.printStackTrace();
					         } finally {
					            try {
					               fileWriter.flush();
					               fileWriter.close();
					            } catch (IOException e) {
					               System.out.println("Error while flushing/closing fileWriter !!!");
					               e.printStackTrace();
					            }
					         }
					      } catch (IOException e) {
					         e.printStackTrace();
					      }
               int m = 0;
               Boolean actualY = false;
               for (Integer method=0; method<methods.length; method++) {
                  if (method == 0)
                    actualY = false;
                  Boolean yes = false;
						      try (BufferedReader brMetrics = new BufferedReader(new FileReader("../../RQ3/CompleteMultiplied/"+project+ "_" + methods[method] + "_Complete.csv"))) {
						         FileWriter fileWriter = null;
						         try {
										    fileWriter = new FileWriter(project+"_"+methods[method]+"_Complete_"+release+"_Train.csv");
						            String linefromcsv = brMetrics.readLine();
						            fileWriter.append(linefromcsv.substring(18,linefromcsv.length()) + "\n");
                        Boolean lt = true;
						            while ( (linefromcsv = brMetrics.readLine()) != null && lt == true) {
						               String[] tokenMetrics = linefromcsv.split(csvSplitBy);
						               Integer versionIndex = Integer.parseInt(tokenMetrics[0]);
						               if ( versionIndex < release) {
                             for (int n = 2; n < 18; n++)
  				                     fileWriter.append(tokenMetrics[n] + ",");
                             fileWriter.append(tokenMetrics[18] + "\n");
                             if (tokenMetrics[tokenMetrics.length -1].equals("Yes")) {
                               yes = true;
                               if (method == 0)
                                 actualY = true;
                             }
						               } else{
                             lt = false;
                           }
						            }
						         } catch (Exception e) {
						            System.out.println("Error in csv writer");
						            e.printStackTrace();
						         } finally {
						            try {
						               fileWriter.flush();
						               fileWriter.close();
						            } catch (IOException e) {
						               System.out.println("Error while flushing/closing fileWriter !!!");
						               e.printStackTrace();
						            }
						         }
						      } catch (IOException e) {
						         e.printStackTrace();
						      }
                  if (yes == true && actualY == true && method!=0) {
	                  System.out.println(methods[method]+": "+ release+"/"+ versions);
							      DataSource sourceTrain = new DataSource(project+ "_" + methods[method] + "_Complete_"+release+"_Train.csv");
							      DataSource sourceTest = new DataSource(project+ "_Actual_Complete_"+release+"_Test.csv");

							      Instances dataTrain = sourceTrain.getDataSet();
							      Instances dataTest = sourceTest.getDataSet();

							      dataTrain.setClassIndex(dataTrain.numAttributes() - 1);
							      dataTest.setClassIndex(dataTrain.numAttributes() - 1);

							      AttributeSelection as = new AttributeSelection();
							      ASSearch asSearch = ASSearch.forName("weka.attributeSelection.ExhaustiveSearch", new String[]{});
							      as.setSearch(asSearch);
	                  CfsSubsetEval asEval = new CfsSubsetEval();
							      as.setEvaluator(asEval);
							      as.SelectAttributes(dataTrain);

							      dataTrain = as.reduceDimensionality(dataTrain);
						        dataTest = as.reduceDimensionality(dataTest);

                    fileWriter1.append( project + "," + release + "," + methods[method] + ",");
										for (Enumeration<Attribute> e = dataTrain.enumerateAttributes(); e.hasMoreElements(); )
										   fileWriter1.append(e.nextElement().name() +",");
                    fileWriter1.append( "\n");


                  }
	                String command = "rm " + project+ "_" + methods[method] + "_Complete_"+release+"_Train.csv";
	                // Read the output
	                Process proc = Runtime.getRuntime().exec(command);
	                proc.waitFor();
               }

            }
         }
       } catch (Exception e) {
          System.out.println("Error in csv writer");
          e.printStackTrace();
       } finally {
          try {
             fileWriter2.flush();
             fileWriter2.close();
             fileWriter1.flush();
             fileWriter1.close();
          } catch (IOException e) {
             System.out.println("Error while flushing/closing fileWriter !!!");
             e.printStackTrace();
          }
       }
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

}
