#!/bin/bash

FILES=IssueJsonFiles/*.json
for f in $FILES
do
  n=${f%_issue*}
  m=${n##*/}
  n=${n##*_}
  newname="FixAndIntroducingCommitsFiles/"
  newname+="$m"
  newname+="_fix_and_introducers_pairs_.json"
  clone="https://github.com/apache/"
  clone+="$n.git"
  repo="$n/"
  git clone $clone
  echo "java -jar szz_find_bug_introducers-0.1.jar -i $f -r $repo -d 1"
  java -jar szz_find_bug_introducers-0.1.jar -i $f -r $repo -d 1
  mv results/fix_and_introducers_pairs.json $newname
  rm -rf issues/
  rm -rf results/
  rm -rf $repo
done
